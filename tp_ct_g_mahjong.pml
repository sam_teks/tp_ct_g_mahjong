<?xml version="1.0" encoding="UTF-8" ?>
<Package name="tp_ct_g_mahjong" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="." xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="home" src="html/css/home.css" />
        <File name="ranking" src="html/css/ranking.css" />
        <File name="FOT-RodinNTLGPro-B" src="html/font/FOT-RodinNTLGPro-B.otf_en" />
        <File name="FOT-RodinNTLGPro-DB" src="html/font/FOT-RodinNTLGPro-DB.woff2_en" />
        <File name="FOT-UDKakugo_LargePr6-B" src="html/font/FOT-UDKakugo_LargePr6-B.woff2_en" />
        <File name="batsu" src="html/images/batsu.png" />
        <File name="choki_none" src="html/images/choki_none.png" />
        <File name="choki_off" src="html/images/choki_off.png" />
        <File name="choki_on" src="html/images/choki_on.png" />
        <File name="end_popup" src="html/images/end_popup.png" />
        <File name="example_bg" src="html/images/example_bg.png" />
        <File name="game_bg_time" src="html/images/game_bg_time.png" />
        <File name="goo_none" src="html/images/goo_none.png" />
        <File name="goo_off" src="html/images/goo_off.png" />
        <File name="goo_on" src="html/images/goo_on.png" />
        <File name="goo_on_ex" src="html/images/goo_on_ex.png" />
        <File name="hard_btn_off" src="html/images/hard_btn_off.png" />
        <File name="hard_btn_on" src="html/images/hard_btn_on.png" />
        <File name="hard_line" src="html/images/hard_line.png" />
        <File name="29_bg" src="html/images/hard_pop/29_bg.png" />
        <File name="29_star" src="html/images/hard_pop/29_star.png" />
        <File name="close_btn_off" src="html/images/hard_pop/close_btn_off.png" />
        <File name="close_btn_on" src="html/images/hard_pop/close_btn_on.png" />
        <File name="xx_guest_difficulty_popup" src="html/images/hard_pop/xx_guest_difficulty_popup.png" />
        <File name="hard_square" src="html/images/hard_square.png" />
        <File name="incorrectanswer" src="html/images/incorrectanswer.png" />
        <File name="incorrectanswer_bg" src="html/images/incorrectanswer_bg.png" />
        <File name="key_btn_off" src="html/images/key_btn_off.png" />
        <File name="key_btn_on" src="html/images/key_btn_on.png" />
        <File name="main_bg_blank" src="html/images/main_bg_blank.png" />
        <File name="maru" src="html/images/maru.png" />
        <File name="minus5sm" src="html/images/minus5sm.png" />
        <File name="normal_btn_off" src="html/images/normal_btn_off.png" />
        <File name="normal_line" src="html/images/normal_line.png" />
        <File name="normal_square" src="html/images/normal_square.png" />
        <File name="par_none" src="html/images/par_none.png" />
        <File name="par_off" src="html/images/par_off.png" />
        <File name="par_on" src="html/images/par_on.png" />
        <File name="q_btn_off" src="html/images/q_btn_off.png" />
        <File name="q_btn_on" src="html/images/q_btn_on.png" />
        <File name="quit_off" src="html/images/quit_off.png" />
        <File name="quit_on" src="html/images/quit_on.png" />
        <File name="A_middle" src="html/images/ranking/A_middle.png" />
        <File name="A_small" src="html/images/ranking/A_small.png" />
        <File name="B_middle" src="html/images/ranking/B_middle.png" />
        <File name="B_small" src="html/images/ranking/B_small.png" />
        <File name="C_middle" src="html/images/ranking/C_middle.png" />
        <File name="C_small" src="html/images/ranking/C_small.png" />
        <File name="D_middle" src="html/images/ranking/D_middle.png" />
        <File name="D_small" src="html/images/ranking/D_small.png" />
        <File name="E_middle" src="html/images/ranking/E_middle.png" />
        <File name="E_small" src="html/images/ranking/E_small.png" />
        <File name="F_middle" src="html/images/ranking/F_middle.png" />
        <File name="F_small" src="html/images/ranking/F_small.png" />
        <File name="G_middle" src="html/images/ranking/G_middle.png" />
        <File name="G_small" src="html/images/ranking/G_small.png" />
        <File name="S_middle" src="html/images/ranking/S_middle.png" />
        <File name="S_small" src="html/images/ranking/S_small.png" />
        <File name="bg" src="html/images/ranking/bg.png" />
        <File name="bul" src="html/images/ranking/bul.png" />
        <File name="hard_off" src="html/images/ranking/hard_off.png" />
        <File name="hard_on" src="html/images/ranking/hard_on.png" />
        <File name="none_bg" src="html/images/ranking/none_bg.png" />
        <File name="normal_off" src="html/images/ranking/normal_off.png" />
        <File name="normal_on" src="html/images/ranking/normal_on.png" />
        <File name="ok_btn_off" src="html/images/ranking/ok_btn_off.png" />
        <File name="ok_btn_on" src="html/images/ranking/ok_btn_on.png" />
        <File name="ranking_plate_2" src="html/images/ranking/ranking_plate_2.png" />
        <File name="result_plate_2" src="html/images/ranking/result_plate_2.png" />
        <File name="setumeiCancel_off" src="html/images/setumeiCancel_off.png" />
        <File name="setumeiCancel_on" src="html/images/setumeiCancel_on.png" />
        <File name="setumei_off" src="html/images/setumei_off.png" />
        <File name="setumei_on" src="html/images/setumei_on.png" />
        <File name="start_off" src="html/images/start_off.png" />
        <File name="start_on" src="html/images/start_on.png" />
        <File name="timeup_popup" src="html/images/timeup_popup.png" />
        <File name="hard_line" src="html/images/title/hard_line.png" />
        <File name="ranking_btn_off" src="html/images/title/ranking_btn_off.png" />
        <File name="ranking_btn_on" src="html/images/title/ranking_btn_on.png" />
        <File name="index" src="html/index.html" />
        <File name="adjust" src="html/js/adjust.js" />
        <File name="configure" src="html/js/configure.js" />
        <File name="jquery.qimhelpers2" src="html/js/jquery.qimhelpers2.js" />
        <File name="main" src="html/js/main.js" />
        <File name="01_title_atack" src="sounds/01_title_atack.ogg" />
        <File name="02_count_default" src="sounds/02_count_default.ogg" />
        <File name="03_count_last" src="sounds/03_count_last.ogg" />
        <File name="04_atari" src="sounds/04_atari.ogg" />
        <File name="05_hazure" src="sounds/05_hazure.ogg" />
        <File name="06_time_up" src="sounds/06_time_up.ogg" />
        <File name="07_syuuryou" src="sounds/07_syuuryou.ogg" />
        <File name="08_click" src="sounds/08_click.ogg" />
        <File name="09_howan" src="sounds/09_howan.ogg" />
        <File name="10_jan" src="sounds/10_jan.ogg" />
        <File name="11_whistle_start" src="sounds/11_whistle_start.ogg" />
        <File name="12_whistle_end" src="sounds/12_whistle_end.ogg" />
        <File name="13_deden" src="sounds/13_deden.ogg" />
        <File name="translation_en_US" src="translations/translation_en_US.qm" />
        <File name="translation_ja_JP" src="translations/translation_ja_JP.qm" />
        <File name="translation_zh_CN" src="translations/translation_zh_CN.qm" />
        <File name="end_popup - Copy" src="html/images/end_popup - Copy.png" />
        <File name="hard_btn_off - Copy" src="html/images/hard_btn_off - Copy.png" />
        <File name="key_btn_on - Copy" src="html/images/key_btn_on - Copy.png" />
        <File name="title" src="html/images/title.jpg" />
        <File name="title" src="html/images/title.png" />
        <File name="button" src="html/css/button.css" />
        <File name="button.css" src="html/css/button.css.map" />
        <File name="button" src="html/css/button.scss" />
        <File name="b0" src="html/images/mahjong/b0.jpg" />
        <File name="b1" src="html/images/mahjong/b1.jpg" />
        <File name="b2" src="html/images/mahjong/b2.jpg" />
        <File name="b3" src="html/images/mahjong/b3.jpg" />
        <File name="b4" src="html/images/mahjong/b4.jpg" />
        <File name="b5" src="html/images/mahjong/b5.jpg" />
        <File name="b6" src="html/images/mahjong/b6.jpg" />
        <File name="b7" src="html/images/mahjong/b7.jpg" />
        <File name="b8" src="html/images/mahjong/b8.jpg" />
        <File name="b9" src="html/images/mahjong/b9.jpg" />
        <File name="c1" src="html/images/mahjong/c1.jpg" />
        <File name="c2" src="html/images/mahjong/c2.jpg" />
        <File name="c3" src="html/images/mahjong/c3.jpg" />
        <File name="c4" src="html/images/mahjong/c4.jpg" />
        <File name="c5" src="html/images/mahjong/c5.jpg" />
        <File name="c6" src="html/images/mahjong/c6.jpg" />
        <File name="c7" src="html/images/mahjong/c7.jpg" />
        <File name="c8" src="html/images/mahjong/c8.jpg" />
        <File name="c9" src="html/images/mahjong/c9.jpg" />
        <File name="d1" src="html/images/mahjong/d1.jpg" />
        <File name="d2" src="html/images/mahjong/d2.jpg" />
        <File name="d3" src="html/images/mahjong/d3.jpg" />
        <File name="d4" src="html/images/mahjong/d4.jpg" />
        <File name="d5" src="html/images/mahjong/d5.jpg" />
        <File name="d6" src="html/images/mahjong/d6.jpg" />
        <File name="d7" src="html/images/mahjong/d7.jpg" />
        <File name="d8" src="html/images/mahjong/d8.jpg" />
        <File name="d9" src="html/images/mahjong/d9.jpg" />
        <File name="img_615526" src="html/images/mahjong/img_615526.jpg" />
        <File name="q0" src="html/images/mahjong/q0.jpg" />
        <File name="mahjongList" src="html/mahjongList.txt" />
        <File name="a1" src="html/images/mahjong/a1.jpg" />
        <File name="a2" src="html/images/mahjong/a2.jpg" />
        <File name="a3" src="html/images/mahjong/a3.jpg" />
        <File name="a4" src="html/images/mahjong/a4.jpg" />
        <File name="a5" src="html/images/mahjong/a5.jpg" />
        <File name="a6" src="html/images/mahjong/a6.jpg" />
        <File name="a7" src="html/images/mahjong/a7.jpg" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
